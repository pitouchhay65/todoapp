import { createSlice } from '@reduxjs/toolkit';
import {getDoument, addData,updateData, removeDocumentByID} from "@/services/http.init";
const todosSlice = createSlice({
  name: 'todos',
  initialState: [],
  reducers: {
    setTodos(state, action) {
      return action.payload;
    },
    addTodo(state, action) {
      const mergeState =  [...state, action.payload];
      return mergeState;
    },
    updateTodo(state, action) {
      const { id, field } = action.payload;
      const todoIndex = state.findIndex((todo) => todo.id === id);
      if (todoIndex !== -1) {
        state[todoIndex][field.key] = field.value;
      }
    },
    updateMarkTODO(state, action) {
        const { id, isComplete } = action.payload;
        const todoIndex = state.findIndex((todo) => todo.id === id);
        if (todoIndex !== -1) {
          state[todoIndex].isComplete = isComplete;
        }
      },
    deleteTodo(state, action) {
      const { id } = action.payload;
      const filterState = state.filter((todo) => todo.id !== id);
      return filterState;
    },
  },
});

export const { setTodos, addTodo, updateTodo,updateMarkTODO, deleteTodo } = todosSlice.actions;

export const fetchTodos = (filter = 'all') => async (dispatch) => {
  try {
    const response = await getDoument('todos',filter);
    dispatch(setTodos(response));
  } catch (error) {
    console.error(error);
  }
};

export const addNewTodo = (text) => async (dispatch) => {
  try {
    const todo = {
        name:text,
        isComplete:false,
        createdAt: new Date()
    };
    const response = await addData('todos',todo);
    todo.id = response.id;
    dispatch(addTodo(todo));
  } catch (error) {
    console.error(error);
  }
};

export const updateExistingTodo = (id, data) => async (dispatch) => {
  try {
    const response = await updateData('todos',id,data);
    if(response) {
        dispatch(updateTodo({ id, field:{key:'name',value:data?.name}}));
    }
  } catch (error) {
    console.error(error);
  }
};

export const markTodo = (id, data) => async (dispatch) => {
    try {
        const response = await updateData('todos',id,data);
        if(response) {

            dispatch(updateTodo({ id,field:{key:'isComplete',value:data?.isComplete}}));
        }
    } catch (error) {
      console.error(error);
    }
};

export const deleteExistingTodo = (id) => async (dispatch) => {
  try {
    const response = await removeDocumentByID('todos',id);
    if(response) {
        dispatch(deleteTodo({ id }));
    }
  } catch (error) {
    console.error(error);
  }
};

export const selectTodos = (state) => state.todos;

export default todosSlice.reducer;
