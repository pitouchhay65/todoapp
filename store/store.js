import { configureStore } from "@reduxjs/toolkit";
import todosReducer from "./slice/todos";
import thunk from 'redux-thunk'
const store = configureStore({
  reducer: {
    todos: todosReducer,
  },
  middleware: [thunk]
});

export default store;
