import Firebase from "@/services/firebase";

import { getFirestore, doc, setDoc, query,where, getDoc,collection,getDocs,addDoc,updateDoc, deleteDoc} from "firebase/firestore";
const db = getFirestore(Firebase);

export const  getDoument = async (c,value= 'all') => {
  let isComplete = [];
  if(value == 'true') {
    isComplete = [true];
  }else {
    isComplete = [false];
  }
  if(value =='all') {
    isComplete = [false,true];
  }
  const q = query(collection(db, c), where('isComplete', 'in', isComplete));
  const querySnapshot = await getDocs(q);

  const cityList = querySnapshot.docs.map(doc => ({...doc.data(), id:doc.id }));
  return cityList;
}

export const addData = async (collect, data) => {
  try {
    const docRef = await addDoc(collection(db, collect), data);
    return docRef; 
  } catch (e) {
    console.error("Error adding document: ", e);
  } 
}
export const updateData = async (collect,id, data) => {
  try {
    const docRef = doc(db, collect, id);
    await updateDoc(docRef,data);
    return true;
  } catch (e) {
    console.error("Error adding document: ", e);
    return false;
  } 
}
export const removeDocumentByID = async (collect, id) => {
  try {
    await deleteDoc(doc(db, collect, id));
    return true; 
  } catch (e) {
    return false; 
  } 
}
export const  getDoumentByID = async (collection, id) => {
  let docRef = doc(db, collection, id);

  let result = null;
  let error = null;

  try {
      result = await getDoc(docRef);
  } catch (e) {
      error = e;
  }

  return { result, error };
}