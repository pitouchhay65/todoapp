
export const setLocalStorage = (key,value) => {
    localStorage.setItem(key,value);
}
export const getLocalStorage = (key) => {
    try {
        return JSON.parse(localStorage.getItem(key));
    } catch (error) {
        return null;
    }
}