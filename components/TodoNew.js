import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addNewTodo, selectTodos } from "@/store/slice/todos";

const NewTodo = () => {
  const todos = useSelector(selectTodos);
  const [text, setText] = useState("");
  const [isError, setIsError] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const dispatch = useDispatch();
  const handleSubmit = (e) => {
    e.preventDefault();
    if(text == '') {
        setIsError(true);
        setErrorMessage('Name is required!');
    }else {
        const chkExist = todos.find(todo => todo.name == text);
        if(chkExist) {
            setIsError(true);
            setErrorMessage('Name already exist!');
        }else {
            setIsError(false);
            dispatch(addNewTodo(text));
            setText("");
        }
    }
  };
  const onChangeText = (e) => {
    setText(e.target.value);
    if(e.target.value == '') {
        setIsError(true);
        setErrorMessage('Name is required!');
    }else {
        setIsError(false)
    }
  }
  return (
    <>
      <form onSubmit={handleSubmit} className="my-5">
        <div className="grid grid-rows-2 grid-flow-col gap-0">
          <div>
            <input
              className="py-1 border-slate-50	 rounded-sm me-2 p-2"
              type="text"
              placeholder="Enter todo text"
              value={text}
              onChange={(e) => onChangeText(e)}
            />
            <button type="submit">Add</button>
          </div>
          <div>
            {isError && <span className="text-red-500">{errorMessage}</span>}
          </div>
        </div>
      </form>
    </>
  );
};

export default NewTodo;
