// src/components/TodoList.js

import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchTodos, selectTodos } from "@/store/slice/todos";
import TodoItem from "./TodoItem";
const TodoList = () => {
  const [isMounted,setIsMounted] = useState(false);
  const todos = useSelector(selectTodos);
  const dispatch = useDispatch();
  useEffect(() => { 
    dispatch(fetchTodos());
  }, [dispatch]);

  useEffect(() => {
    setIsMounted(true)
  },[]);
  const onFilter = (e) => {
    dispatch(fetchTodos(e.target.value));
  }
  return (
    isMounted && (
      <>
        <div className="flex justify-end w-full">
          <select onChange={onFilter} name="filter" id="filter" className="p-2">
            <option value="all">All</option>
            <option value="true">Completed</option>
            <option value="false">Incomplete</option>
          </select>
        </div>
        <table className="w-full	 table-auto my-5 rounded-t-xl overflow-hidden bg-gradient-to-r from-emerald-50 to-teal-100 p-1">
          <thead>
            <tr>
              <th className="px-4 py-2">Name</th>
              <th className="px-4 py-2">isCompleted</th>
              <th className="px-4 py-2" colSpan={2}>
                Actions
              </th>
            </tr>
          </thead>
          <tbody>
            {todos.length > 0
              ? todos.map((todo,key) => <TodoItem key={key} todo={todo} />)
              : <tr><td colSpan={4} className="text-center">No data available</td></tr>}
          </tbody>
        </table>
      </>
    )
  );
};
export default TodoList;
