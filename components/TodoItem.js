import { useState } from "react";
import { useDispatch } from "react-redux";
import {
  updateExistingTodo,
  deleteExistingTodo,
  markTodo,
} from "@/store/slice/todos";

const TodoItem = ({ todo }) => {
  const [editingText, setEditingText] = useState(todo.name);
  const [isEditing, setIsEditing] = useState(false);
  const dispatch = useDispatch();

  const handleUpdateClick = () => {
    dispatch(updateExistingTodo(todo.id, {name:editingText}));
    setIsEditing(false);
  };

  const handleDeleteClick = () => {
    dispatch(deleteExistingTodo(todo.id));
  };
  const onMarkTODO = (e,todo) => {
    dispatch(markTodo(todo.id, {isComplete:e.target.checked}));
  };
  return (
      isEditing ? (
        <tr>
          <td>
            <input
              type="text"
              value={editingText}
              onChange={(e) => setEditingText(e.target.value)}
            />
          </td>
          <td className="px-5 py-2 font-medium">
            <input type="checkbox" value={todo.isComplete} checked={todo.isComplete} onChange={(e) => onMarkTODO(e)} />
          </td>
          <td>
            <button onClick={handleUpdateClick}>Update</button>
          </td>
          <td>
            <button onClick={() => setIsEditing(false)}>Cancel</button>
          </td>
        </tr>
      ) : (
        <tr>
          <td className="px-5 py-2 font-medium">{todo.name}</td>
          <td className="px-5 py-2 font-medium">
            <input type="checkbox" value={todo.isComplete} checked={todo.isComplete}  onChange={(e) => onMarkTODO(e,todo)} />
          </td>
          <td>
            <button onClick={() => setIsEditing(true)}>Edit</button>
          </td>
          <td>
            <button onClick={handleDeleteClick}>Delete</button>
          </td>
        </tr>
      )
  );
};
export default TodoItem;
