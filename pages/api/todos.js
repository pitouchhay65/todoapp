// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default function handler(req, res) {
    res.status(200).json(
        [
            {
                id:1,
                text:'Todo',
                isComplete:false,
                createdAt: new Date()
            },
            {
                id:2,
                text:'Todo2',
                isComplete:false,
                createdAt: new Date()
            }
        ]
    )
}
  