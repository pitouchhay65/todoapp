import Image from "next/image";
import { Inter } from "next/font/google";

const inter = Inter({ subsets: ["latin"] });
import TodoList from "@/components/TodoList";
import NewTodo from "@/components/TodoNew";

export default function Home() {
  return (
    <main
      className={`flex min-h-screen flex-col items-center p-24 ${inter.className}`}>
      <h1>Todo List</h1>
      <NewTodo />
      <TodoList />
    </main>
  );
}
