import '@/styles/globals.css'
import store from '@/store/store';
import { Provider } from 'react-redux';
import Firebase from '@/services/firebase';


export default function App({ Component, pageProps }) {
  return <Provider store={store}>
    <Component {...pageProps} />
</Provider>
}
